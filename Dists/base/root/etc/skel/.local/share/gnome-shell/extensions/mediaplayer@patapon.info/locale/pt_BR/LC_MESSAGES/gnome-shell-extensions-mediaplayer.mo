��          �      |      �     �  '        *  =   >     |     �  	   �  I   �     �  (   �  %   $  #   J     n  C   v     �     �     �     �     �     �     �  ~  �     z  '   �      �  H   �     &     /  	   7  K   A      �  '   �  .   �  +        1  E   8     ~     �     �     �     �     �     �                                                         
                 	                       Album cover size Allow to start the default media player Display song rating Display the currently playing song's rating on a 0 to 5 scale Paused Playing Playlists Runs the default mediaplayer by clicking on the media player status icon. Show media player playlists Show the media player in the volume menu Show the media player position slider Show the media player volume slider Stopped The size of the cover displayed in the menu. Default is 80px width. Unknown Album Unknown Artist Unknown Title Volume by from rating Project-Id-Version: gnome-shell-extension-mediaplayer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-05-03 10:26+0200
PO-Revision-Date: 2012-05-21 23:10-0300
Last-Translator: Elder Marco <eldermarco@gmail.com>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Portuguese
X-Poedit-Country: BRAZIL
 Tamanho da capa do álbum Permitir iniciar o media player padrão Mostrar a avaliação da música Mostra a avaliação da música que está tocando em uma escala de 0 a 5 Em pausa Tocando Playlists Executa o media player padrão clicando no ícone de status do media player Exibir playlists do media player Exibir o media player no menu de volume Exibir o controle de posição do media player Exibir o controle de volume do media player Parado O tamanho da capa exibida no menu. Por padrão, 80 pixels de largura. Álbum Desconhecido Artista Deconhecido Título Desconhecido Volume por de avaliação 