
const St = imports.gi.St;
const Main = imports.ui.main;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Tweener = imports.ui.tweener;
const Mainloop = imports.mainloop;

const TOOLTIP_LABEL_SHOW_TIME = 0.15;
const TOOLTIP_LABEL_HIDE_TIME = 0.1;
const TOOLTIP_HOVER_TIMEOUT = 300;

// used to restore monkey patched function on disable
let old_addItem;
// used to disconnect events on disable
let tooltips;

/*Tooltip borrowed from dash and internet (fpmurphy), and made into a class*/
const Tooltip = new Lang.Class({
    Name: 'Tooltip',

    _init: function(actr, txt){
        this.actor = actr;
        this._text = txt;
        this._labelTimeoutId = 0;
        this._connection = this.actor.connect('notify::hover',
            Lang.bind(this, this._onHover));
    },

    _disconnect: function(){
        this.actor.disconnect(this._connection);
    },

    _onHover: function () {
        if (this.actor.get_hover()) {
            if (this._labelTimeoutId == 0) {
                this._labelTimeoutId = Mainloop.timeout_add(
                    TOOLTIP_HOVER_TIMEOUT,
                    Lang.bind(this, function() {
                        this._showTooltip();
                        return false;
                    })
                );
            }
        } else {
            if (this._labelTimeoutId > 0)
                Mainloop.source_remove(this._labelTimeoutId);
            this._labelTimeoutId = 0;
            this._hideTooltip();
        }
    },

    _hideTooltip: function() {
        if (this.label){
            Tweener.addTween(this.label, {
                opacity: 0,
                time: TOOLTIP_LABEL_HIDE_TIME,
                transition: 'easeOutQuad',
                onComplete: Lang.bind(this, function() {
                    Main.uiGroup.remove_actor(this.label);
                    this.label = null;
                })
            });
        }
    },

    _showTooltip: function() {
        if (!this._text)
            return;
        // text might be fully displayed, check if tooltip is needed
        if (this.actor._delegate.app){
            //applications overview
            var should_display = this.actor._delegate.icon.label.get_clutter_text().get_layout().is_ellipsized();
        }else if (this.actor._delegate._content._delegate){
            //app and settings searchs results
            var should_display = this.actor._delegate._content._delegate.icon.label.get_clutter_text().get_layout().is_ellipsized();
        }else{
            //locations search results
            var should_display = this.actor.label_actor.get_clutter_text().get_layout().is_ellipsized();
        }
        if (!should_display)
            return;

        if (!this.label) {
            this.label = new St.Label({style_class: 'tooltip dash-label',
                text: this._text
            });
            Main.uiGroup.add_actor(this.label);
        }

        [stageX, stageY] = this.actor.get_transformed_position();
        [iconWidth, iconHeight] = this.actor.get_transformed_size();

        var y = stageY + iconHeight;
        var x = stageX - Math.round((this.label.get_width() - iconWidth)/2);
        this.label.opacity = 0;
        this.label.set_position(x, y);
        Tweener.addTween(this.label,{
            opacity: 255,
            time: TOOLTIP_LABEL_SHOW_TIME,
            transition: 'easeOutQuad',
        });
    },

});

function init() {}

function enable() {
    old_addItem = imports.ui.iconGrid.IconGrid.prototype.addItem;
    tooltips = new Array();

    // It's for enabling tooltips after _appIcons was populated
    var appIcons = Main.overview._viewSelector._tabs[1].page.get_child()
            .get_child()._delegate._view._appIcons;
    for (var i in appIcons) {
        var actor = appIcons[i].actor;
        var tooltip = new Tooltip(actor, actor._delegate.app.get_name());
        tooltips.push(tooltip);
    }

    // this monkeypatching is for the first time and for the search overview
    // wanda and contacts search providers doesn't use this (can't provide tooltips for those yet)
    imports.ui.iconGrid.IconGrid.prototype.addItem = function(actor){
        if (actor._delegate.app){
            //app icons
            var tooltip = new Tooltip(actor, actor._delegate.app.get_name());
        }else{
            //search results
            var tooltip = new Tooltip(actor, actor._delegate.metaInfo['name']);
        }
        tooltips.push(tooltip);

        // original part of the function I'm overwriting
        this._grid.add_actor(actor);
    };
}

function disable() {
    imports.ui.iconGrid.IconGrid.prototype.addItem = old_addItem;
    for (var i = 0; i < tooltips.length; i++) {
        //if searched much some of these should not exist anymore, not sure what to do
        tooltips[i]._disconnect();
    }
    tooltips=null;
}
