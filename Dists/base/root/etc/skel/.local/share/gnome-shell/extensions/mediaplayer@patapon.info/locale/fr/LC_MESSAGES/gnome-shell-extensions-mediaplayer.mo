��          �      |      �     �  '        *  =   >     |     �  	   �  I   �     �  (   �  %   $  #   J     n  C   v     �     �     �     �     �     �     �  �  �      }  6   �     �  D   �     9     B     K  P   ]  5   �  6   �  7     5   S     �  T   �     �     �                         !                                                         
                 	                       Album cover size Allow to start the default media player Display song rating Display the currently playing song's rating on a 0 to 5 scale Paused Playing Playlists Runs the default mediaplayer by clicking on the media player status icon. Show media player playlists Show the media player in the volume menu Show the media player position slider Show the media player volume slider Stopped The size of the cover displayed in the menu. Default is 80px width. Unknown Album Unknown Artist Unknown Title Volume by from rating Project-Id-Version: gnome-shell-extension-mediaplayer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-05-03 10:26+0200
PO-Revision-Date: 2012-05-01 20:02-0500
Last-Translator: Charles Brière <charlesbriere.flatzo@gmail.com>
Language-Team: Jean-Philippe Braun <eon@patapon.info>
Language: French
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Taille de la pochette de l'album Permettre de lancer le lecteur multimédia par défaut Afficher la note de la chanson Afficher la note de la chanson en lecture sur une échelle de 0 à 5 En pause En cours Listes de lecture Lance le lecteur multimédia par défaut en cliquant sur l'icone de l'extension. Afficher les listes de lecture du lecteur multimédia Afficher le lecteur multimédia dans le menu du volume Afficher la barre de progression du lecteur multimédia Afficher le controle du volume du lecteur multimédia Stoppé La taille de la pochette affichée dans le menu. La largeur par défaut est de 80px. Album inconnu Artiste inconnu Titre inconnu Volume par sur note 