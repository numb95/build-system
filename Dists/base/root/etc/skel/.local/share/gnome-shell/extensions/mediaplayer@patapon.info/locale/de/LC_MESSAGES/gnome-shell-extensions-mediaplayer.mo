��          �      L      �     �  '   �     �       	   	  I        ]  (   y  %   �  #   �     �  C   �     8     F     U     c     j     m  m  r     �  ,   �     $     -  	   ;  ?   E     �  '   �     �     �  
   �  B   �     9     K     a     s          �                                                                             	                
       Album cover size Allow to start the default media player Paused Playing Playlists Runs the default mediaplayer by clicking on the media player status icon. Show media player playlists Show the media player in the volume menu Show the media player position slider Show the media player volume slider Stopped The size of the cover displayed in the menu. Default is 80px width. Unknown Album Unknown Artist Unknown Title Volume by from Project-Id-Version: gnome-shell-extension-mediaplayer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-05-03 10:26+0200
PO-Revision-Date: 2012-04-04 09:00+0200
Last-Translator: Frederik Hahne <frederik.hahne@googlemail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: German
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Größe der Albumcover Erlauben den Standard-Mediaplayer zu starten Pausiert Spielt gerade Playlists Den Standard-Mediaplayer beim klick auf das Status Icon starten Playlists anzeigen Mediaplayer im Lautstärkemenu anzeigen Slider anzeigen Lautstärkeregler anzeigen Angehalten Größe der Albumcover im Menu. Standard ist eine Breite von 80px. Unbekanntes Album Unbekannter Interpret Unbekannter Titel Lautstärke von aus 