��          �      L      �     �  '   �     �       	   	  I        ]  (   y  %   �  #   �     �  C   �     8     F     U     c     j     m  �  r     .  $   F  
   k     v     �  V   �     �  .   
  (   9  -   b  
   �  _   �     �     
          -     :     F                                                                             	                
       Album cover size Allow to start the default media player Paused Playing Playlists Runs the default mediaplayer by clicking on the media player status icon. Show media player playlists Show the media player in the volume menu Show the media player position slider Show the media player volume slider Stopped The size of the cover displayed in the menu. Default is 80px width. Unknown Album Unknown Artist Unknown Title Volume by from Project-Id-Version: gnome-shell-extension-mediaplayer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-05-03 10:26+0200
PO-Revision-Date: 2012-04-08 11:54+0200
Last-Translator: Piotr Sokół <psokol@jabster.pl>
Language-Team: polski <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Plural-Forms: nplurals=3; plural=((n==1) ? 0 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2));
 Rozmiar okładki albumu Uruchamianie domyślnego odtwarzacza Wstrzymano Odtwarzanie Listy odtwarzania Uruchamia domyślny odtwarzacz multimediów po kliknięciu w ikonę stanu rozszerzenia Wyświetlanie list odtwarzania Wyświetlanie rozszerzenia w menu głośności Wyświetlanie suwaka pozycji odtwarzania Wyświetlanie suwaka głośności odtwarzacza Zatrzymano Określa rozmiar okładki wyświetlanej w menu. Domyślna szerokość obrazu wynosi 80 pikseli. Nieznany album Nieznany wykonawca Nieznany tytuł Głośność w wykonaniu z albumu 