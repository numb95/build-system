echo 'Configuring gnome-shell ...'
echo 'enable all extensions in ~/.local/share/gnome-shell/extensions'
echo "[org.gnome.shell]" >> /usr/share/glib-2.0/schemas/arios.gschema.override


find  /etc/skel/.local/share/gnome-shell/extensions/  /usr/share/gnome-shell/extensions/ -maxdepth 1 -mindepth 1 -printf "%f\n" -type d | xargs -I{} echo "'{}'" | sed ':a;N;$!ba;s/\n/,/g' | xargs -0 -I{} echo "enabled-extensions=[{}]" | sed ':a;N;$!ba;s/\n//g' >> /usr/share/glib-2.0/schemas/arios.gschema.override
glib-compile-schemas /usr/share/glib-2.0/schemas
#---------------------------------------------------------------------------------------

echo "configuring splash"
sudo update-alternatives --install /lib/plymouth/themes/default.plymouth default.plymouth /lib/plymouth/themes/AriOS/AriOS.plymouth 100
sudo update-alternatives --config default.plymouth
sudo update-initramfs -u
